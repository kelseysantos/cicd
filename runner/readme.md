# Configuracoes

Não esquecer de ativar o **Privileged = true**.

https://forum.gitlab.com/t/docker-machine-docker-in-docker-cannot-connect-to-the-docker-daemon-at-tcp-localhost-2375-is-the-docker-daemon-running/72467

```conf
[[runners]]
  name = "runnerdocker"
  url = "https://gitlab.com"
  id = 38468088
  token = "glrt-8zmLY-hwmxtg-36KjdyZ"
  token_obtained_at = 2024-06-13T21:15:06Z
  token_expires_at = 0001-01-01T00:00:00Z
  executor = "docker"
  [runners.custom_build_dir]
  [runners.cache]
    MaxUploadedArchiveSize = 0
    [runners.cache.s3]
    [runners.cache.gcs]
    [runners.cache.azure]
  [runners.docker]
    tls_verify = false
    image = "ruby:2.7"
    privileged = true
    disable_entrypoint_overwrite = false
    oom_kill_disable = false
    disable_cache = false
    volumes = ["/cache"]
    shm_size = 0
    network_mtu = 0
```